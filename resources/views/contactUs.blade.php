@extends('layouts.app')
@include('includes.header')
@section('content')
    @include('includes.nav')
    <main class="contact">

        <section class="section hero">
            <div class="hero-padding">
                <div class="text-center">
                    <div class="col">
                        <h2 class="d-none d-md-none d-lg-block">{{ $data['pageInfo']['heroTitleH2'] }}</h2>
                        <h1 class="page-title">{{ $data['pageInfo']['heroTitleH1'] }}</h1>
                        {{url()->current()}}
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5 mt-5">
            <div id="contact" class="container mt-5">
                <div class="row">
                    <div class="col-md-6 mb-5">
                        @if (session()->has('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @else
                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <h4>For a free quote please contact us using the form below:</h4>
                            <br>
                            @include('includes.contact-form')
                        @endif
                    </div>
                    <div class="col-md-6 mb-5">
                        @foreach($data['contactInfo'] as $key => $contact)
                            <h5>
                                {{ $contact->name }} :
                                <a href="{{ $contact->class }}:{{ $contact->description  }}">
                                    {{ $contact->description }}
                                </a>
                            </h5>
                        @endforeach
                        <br>
                        <h4>WHAT WE WILL NEED FROM YOU…</h4>
                        <br>
                        <p class="lead-text">
                            In order for us to supply an accurate quote and timescale for your website project here are
                            some questions you will need to consider..
                        </p>
                        <ul>
                            <li>Do you know how many pages you will require and what the content will be?</li>
                            <li>
                                Do you have any preferences on
                                <a href="https://coolors.co/palettes/trending"
                                   target="_blank">
                                    colour scheme
                                </a>
                                and layout?
                            </li>
                            <li>Are there any websites that have a similar concept to the site you require?</li>
                            <li>Do you wish to
                                <a href="https://www.atlanticbt.com/insights/how-much-does-ecommerce-website-cost/"
                                   target="_blank">
                                    sell
                                </a>
                                products/services from the website, if so, how many products will need to be listed?
                            </li>
                            <li>Do you require a content management system to enable you to make changes to the site
                                yourself?
                            </li>
                            <li>Do you require a domain name and/or website hosting to be set up?</li>
                            <li>What is your budget and when does the work need to be completed by?</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
