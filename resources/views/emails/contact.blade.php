<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    From : {{ $email }}
</div>

<div>
    Name : {{ $firstName }} {{ $lastName }}
</div>

<div>
    Phone Number : {{ $phone }}
</div>

@if( $currentWebsite )
    <div>
        current Website : <a href="{{ $currentWebsite }}" target="_blank">Link to Website</a>
    </div>
@endif

<div>
    Who are your clients and how do you help them? :
</div>
<div>
    {!! $businessClients !!}
</div>

<div>
    Desired timeline:
    @if($deadline == 1)
        1 month
    @elseif($deadline == 2)
        2 months
    @elseif($deadline == 3)
        3 months
    @elseif($deadline == 4)
        More than 4 months
    @elseif($deadline == 5)

    @endif
</div>

<div>
    Budget? :
    @if($budget == 1)
        £200 to £499
    @elseif($budget == 2)
        £500 to £749
    @elseif($budget == 3)
        £750 to £999
    @elseif($budget == 4)
        £1000 to £1999
    @elseif($budget == 5)
        £2000+
    @elseif($budget == 6)

    @endif
</div>

<div>
    Do you have content ready for your website? : {{ $contentReady == 1 ? 'No' : 'Yes' }}
</div>

<div>
    What are your goals for your new website? :
</div>
<div>
    {!! $websiteGoals !!}
</div>

</body>
</html>
