<div class="fixed-top">

<div class="contact-bar d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <a class="mr-2" href="/">HOME</a>
                |
                <a class="mx-2" href="/">
                    <i class="fas fa-phone-alt mr-2"></i>
                    07932410231
                </a>
                |
                <a class="mx-2" href="/">
                    <i class="fas fa-envelope mr-2"></i>
                    contact@pixlquick.com
                </a>
            </div>
            <div class="col-6">
                <div class="float-right">
                    <a class="mx-2" href="">
                        <i class="fab fa-facebook-f fa-lg"></i>
                    </a>
                    |
                    <a class="mx-2" href="">
                        <i class="fab fa-instagram fa-lg"></i>
                    </a>
                    |
                    <a class="ml-2" href="">
                        <i class="fab fa-twitter fa-lg"></i>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<nav class="navbar navbar-expand-md bg-dark">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>

        <div class="d-none d-md-block" id="navbarSupportedContent">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @foreach($data['navLinks'] as $link)
                    <li class="nav-item">
                        <a href="{{ $link->slug }}"
                           class="nav-link {{ Request::path() == $link->slug ? 'active-nav' : '' }}">
                            {{ $link->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

        <a class="btn btn-outline-primary d-block d-md-none" data-toggle="collapse" href="#navMobile" role="button"
           aria-expanded="false">
            <i class="fas fa-bars"></i>
            Menu
        </a>

    </div>

    <div class="d-block d-md-none nav-mobile">
        <div class="collapse" id="navMobile">
            <ul class="navbar-nav p-3">
                @foreach($data['navLinks'] as $link)
                    <li class="nav-item my-2 mobile-nav-link">
                        <a class="nav-link" href="{{ $link->slug }}"
                           class="{{ Request::path() == $link->slug ? 'active-nav' : '' }}">
                            {{ $link->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

</nav>

</div>
