<form id="contact-form" action="/contact-form/submit" method="post">
    @csrf

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="first_name">
                First Name *
            </label>
            <input type="text"
                   class="form-control"
                   id="first_name"
                   name="first_name"
                   placeholder="Enter your first name">
        </div>
        <div class="form-group col-md-12">
            <label for="last_name">
                Last Name *
            </label>
            <input type="text"
                   class="form-control"
                   id="last_name"
                   name="last_name"
                   placeholder="Enter your last name">
        </div>
    </div>

    <div class="form-row my-3">
        <div class="form-group col-md-12">
            <label for="email">
                Email *
            </label>
            <input type="text"
                   class="form-control"
                   id="email"
                   name="email"
                   placeholder="Enter your email address">
        </div>
        <div class="form-group col-md-12">
            <label for="phone">
                Phone *
            </label>
            <input type="text"
                   class="form-control"
                   id="phone"
                   name="phone"
                   placeholder="Enter your contact number">
        </div>
    </div>

    <div class="form-row my-3">
        <div class="form-group col-md-12">
            <label for="deadline">
                Desired timeline *
            </label>
            <select class="form-control" name="deadline">
                <option value="1">1 month</option>
                <option value="2">2 months</option>
                <option value="3">3 months</option>
                <option value="4">More than 4 months</option>
            </select>
        </div>
        <div class="form-group col-md-12">
            <label for="content_ready">
                Do you have content ready for your website? *
            </label>
            <br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="content_ready" id="content_ready_no" value="1">
                <label class="form-check-label" for="content_ready_no">No</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="content_ready" id="content_ready_yes" value="2">
                <label class="form-check-label" for="content_ready_yes">Yes</label>
            </div>
        </div>
    </div>

    <div class="form-row my-3">
        <div class="form-group col-md-12">
            <label for="current_website">
                Current Website
            </label>
            <input type="text"
                   class="form-control"
                   id="current_website"
                   name="current_website"
                   placeholder="Enter your current website (if you have one)">
        </div>
        <div class="form-group col-md-12">
            <label for="budget">
                Budget? *
            </label>
            <select class="form-control" name="budget">
                <option value="1">£200 to £499</option>
                <option value="2">£500 to £749</option>
                <option value="3">£750 to £999</option>
                <option value="4">£1000 to £1999</option>
                <option value="5">£2000+</option>
            </select>
        </div>
    </div>

    <div class="form-group my-3">
        <label for="business_clients">
            Who are your clients and how do you help them? *
        </label>
        <textarea class="form-control"
                  id="business_clients"
                  name="business_clients"
                  rows="3"></textarea>
    </div>

    <div class="form-group my-3">
        <label for="website_goals">
            What are your goals for your new website? *
        </label>
        <textarea class="form-control"
                  id="website_goals"
                  name="website_goals"
                  rows="3"></textarea>
    </div>

    <div class="g-recaptcha my-3" data-sitekey="6LeE0KUZAAAAAJjbP5ptl84UAqJH5hdakl68BB1W"></div>

    <button type="submit" class="btn btn-success px-5 py-3 mt-3 shadow">Send Message</button>

</form>
