@extends('layouts.app')
@include('includes.header')
@section('content')
    @include('includes.nav')
    <main class="home">

        <section class="section hero hero-bg">
            <div class="hero-padding">
                <div class="container">
                    <div class="row text-center">
                        <div class="col">
                            <h1 class="page-title">{{ $data['pageInfo']['heroTitleH1'] }}</h1>
                            <h2>{{ $data['pageInfo']['heroTitleH2'] }}</h2>
                        </div>
                    </div>
                    <div class="hero-cta">
                        <div class="row">
                            <div class="mx-auto">
                                <a href="/services"
                                   class="btn btn-primary contact-us shadow py-3 px-4 mr-1">
                                    Our services
                                </a>
                                <a href="/contact-us"
                                   class="btn btn-outline-primary contact-us shadow py-3 px-4 ml-1">
                                    Get in touch
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <h3>What We Do</h3>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
