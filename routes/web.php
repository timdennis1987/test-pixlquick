<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'websiteController@homePage')->name('home');
Route::get('contact-us', 'websiteController@contactUs');

//Contact Form Section
Route::get('contact-form', 'ContactFormController@showForm');
Route::post('contact-form/submit', 'ContactFormController@submitContactForm');
