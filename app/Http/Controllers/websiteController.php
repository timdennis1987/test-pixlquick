<?php

namespace App\Http\Controllers;

use App\Option;
use App\Page;
use Illuminate\Http\Request;
use App\WpSite;

class websiteController extends Controller
{
    public function wp()
    {
        return new WpSite;
    }

    public function getPageData($slug)
    {
        $data['pageInfo'] = $this->wp()->wpPageData($slug);
        $data['contactInfo'] = Option::where('type', 1)->where('display', 1)->get();
        $data['navLinks'] = Page::where('display', 1)->get();

        return $data;
    }

    public function homePage($slug = 'home')
    {
        return view('home', [
            'data' => $this->getPageData($slug)
        ]);
    }

    public function contactUs($slug = 'contact-us')
    {
        return view('contactUs', [
            'data' => $this->getPageData($slug)
        ]);
    }
}
