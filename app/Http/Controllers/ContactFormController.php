<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    public function showForm()
    {
        return view('includes.contact-form');
    }

    public function submitContactForm(Request $request)
    {
        $gRecaptcha = $request->input('g-recaptcha-response');
        $csrfToken = $request->input('_token');

        $data = request()->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|min:5',
            'content_ready' => 'required',
            'business_clients' => 'required|min:2',
            'website_goals' => 'required|min:2',
        ]);

        $emailTo = Option::where('value', 'main_email')->first(['description']);
        $ip = request()->ip();

        if (!$gRecaptcha) {
            return redirect()->back()->with('error', 'Need to check that you are not a robot!');
        }

        $data = [
            'email' => $request->email,
            'firstName' => $request->first_name,
            'lastName' => $request->last_name,
            'phone' => $request->phone,
            'currentWebsite' => $request->current_website,
            'businessClients' => $request->business_clients,
            'deadline' => $request->deadline,
            'contentReady' => $request->content_ready,
            'websiteGoals' => $request->website_goals,
            'budget' => $request->budget
        ];

        Mail::send('emails.contact', $data, function ($message) use ($data, $emailTo) {

            $message->from($data['email']);
            $message->to($emailTo->description);
            $message->subject('New application for website');
        });

        Contact::create([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'current_website' => $request->current_website,
            'business_clients' => $request->business_clients,
            'deadline' => $request->deadline,
            'content_ready' => $request->content_ready,
            'website_goals' => $request->website_goals,
            'budget' => $request->budget,
            'ip' => $ip,
            'g_recaptcha' => $gRecaptcha,
            'csrf_token' => $csrfToken
        ]);

        return redirect()->back()->with('success', 'Thank you for your message, we will be in contact shortly');
    }
}
